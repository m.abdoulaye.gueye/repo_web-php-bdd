TD WEB PHP BDD :

apres avoir recuperer le zip contenant un fichier php et sql , 
Etape 1: 

je configure le fichier nginx.conf :

server {
    listen 80;
    server_name localhost;
    root /var/www/html;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}

je cree mon DockerFile :
FROM nginx:latest

COPY nginx.conf /etc/nginx/conf.d/default.conf

et je finis par faire mon docker-compose.yml : en y rajoutant l image et le volume de mon fichier php
version: '3'
services:
  php:
    image: php:7.4-fpm
    volumes:
      - ./php:/var/www/html
    container_name: php

  nginx:
    build:
      context: ./nginx
    container_name: nginx
    ports:
      - "80:80"
    volumes:
      - ./php:/var/www/html
    depends_on:
      - php


Etape 2:

je modifie mon fichier docker-compose.yml pour rajouter la base de donnees mysql

version: '3'
services:
  php:
    image: php:7.4-fpm
    volumes:
      - ./php:/var/www/html
    container_name: php

  nginx:
    build:
      context: ./nginx
    container_name: nginx
    ports:
      - "8080:80"  # Utilisation du port 8080 pour Nginx
    volumes:
      - ./php:/var/www/html
    depends_on:
      - php

  db:
    image: mariadb:latest  # Utilisation de l'image MariaDB
    container_name: mariadb
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: rootpassword  # Mot de passe root pour MySQL/MariaDB
      MYSQL_DATABASE: mydatabase  # Nom de la base de données à créer
      MYSQL_USER: dbuser  # Utilisateur pour accéder à la base de données
      MYSQL_PASSWORD: dbpassword  # Mot de passe de l'utilisateur
    ports:
      - "3306:3306"  # Mapping du port MySQL/MariaDB
    volumes:
      - mariadb_data:/var/lib/mysql  # Volume pour stocker les données de la base de données

volumes:
  mariadb_data:
    driver: local

et ensuite je modifie mon fichier php pour qu il puisse se connecter a la base de donnees
en rajoutant les lignes suivantes :

$pdo = new PDO("mysql:host=db;dbname=mydatabase", "dbuser", "dbpassword");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
a la place de :
  $pdo = new PDO("XXX", "YYY", "ZZZ");

Ensuite je lance le Docker avec la commande suivante :
docker compose up -d;


TD GITHUB RUNNER :

Etape 1 : créer le docker-compose.yml pour le runner 

créer un dossier pour le runner

créer le fichier yaml

renseigner  le token et l'url du repo github web php

config :

...

REPO_URL: https://github.com/kteyssere/tp-web-php-bdd

RUNNER_NAME: linux

RUNNER_TOKEN: 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

...


Etape 2 : faire un ci.yml pour le projet github web php

se rendre dans le repo du projet
créer le fichier yaml dans un nouveau dossier .github/workflows/
y ajouter les actions a effectuer

Etape 3 : push les modifications du projet github et lancer le runner

cd ci_cd_teyssere_gueye/TDWebPhpBdd/GithubRunner
docker compose up : démarre tous les services définis dans un fichier docker-compose.yml, créant et lançant les conteneurs nécessaires dans le processus
